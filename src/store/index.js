import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: [],
    shoppingCart: {
      products: [],
      productCount: 0,
      totalPrice: 0
    },
    shoppingCartOpen: false
  },
  mutations: {
    setShoppingCartOpen(state, payload) {
      state.shoppingCartOpen = payload;
    },
    addProductToShoppingCart(state, newProduct) {
      if (state.shoppingCart.length == 0) {
        newProduct.amount = 1;
        state.shoppingCart.products.push(newProduct);
        state.shoppingCart.productCount++;
        state.shoppingCart.totalPrice += newProduct.price;
      } else {
        // Check if shopping cart already has the same product
        const index = state.shoppingCart.products.findIndex(p => p.id === newProduct.id);
        if (index == -1) {
          newProduct.amount = 1;
          state.shoppingCart.products.push(newProduct);
          state.shoppingCart.productCount++;
          state.shoppingCart.totalPrice += newProduct.price;
        } else {
          state.shoppingCart.products[index].amount++;
          state.shoppingCart.productCount++;
          state.shoppingCart.totalPrice += newProduct.price;
        }
      }
    },
    removeProductFromShoppingCart(state, productId) {
      const index = state.shoppingCart.products.findIndex(p => p.id === productId);
      if (index < 0) return;

      if (state.shoppingCart.products[index].amount <= 1) {
        state.shoppingCart.productCount--;
        state.shoppingCart.totalPrice -= state.shoppingCart.products[index].price;
        state.shoppingCart.products.splice(index, 1);
      } else {
        state.shoppingCart.products[index].amount--;
        state.shoppingCart.productCount--;
        state.shoppingCart.totalPrice -= state.shoppingCart.products[index].price;
      }
    },
    setProducts(state, payload) {
      state.products = payload;
    }
  },
  actions: {
    async setProducts(state) {
      // Here you would use fetch to get data from a server, but in this demo I read the products.js file

      const PRODUCTS_LIST = require('./products');
      setTimeout(() => {
        state.commit("setProducts", PRODUCTS_LIST.products);
      }, 100);
    }
  },
  modules: {
  },
  getters: {
    products: state => state.products,
    shoppingCartOpen: state => state.shoppingCartOpen,
    shoppingCart: state => state.shoppingCart,
  }
})
